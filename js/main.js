$(document).ready(function(){
    $('.slider').slick({
        // autoplay: true,
        initialSlide: 0,
        centerMode: true,
        variableWidth: true,
        infinite: true,
        dots: false,
        slidesToScroll: 5,
        focusOnSelect: true
    });
    initLoadImg();
    OpenBox({
        wrap: '#nav',
        link: '.toogle-menu',
        box: '.menu-holder',
        openClass: 'open'
    });
    OpenBox({
        wrap: '#nav .menu li',
        link: '.arrow-menu',
        box: '.drop',
        openClass: 'open'
    });
    OpenBox({
        wrap: '#nav',
        link: '.nav-overlay',
        box: '.menu-holder',
        openClass: 'open'
    });
    listCounter();
		$('.slide-hedges').gallery({
        oneSlide: true
	});
    gallerySelect();
});
$(window).on('load', function(){
    resizeMap();
});
function initLoadImg () {
    $('body').each(function(){
        var hold = $(this);
        var box = hold.find('[data-url]');

        var _scroll = function(){
            box.each(function(){
                if($(window).scrollTop() + $(window).height()*2 > $(this).offset().top ){
                    if(this.tagName.toLowerCase() == 'img'){
                        $(this).attr('src', $(this).data('url'));
                    }
                    else{
                        $(this).css({
                            'background-image': $(this).data('url')
                        });
                    }
                    $(this).removeAttr('data-url').trigger('loaded');
                    box = hold.find('[data-url]');
                }
            });
        }
        _scroll();
        $(window).bind('scroll', _scroll);
    });
}
function OpenBox(obj){
    $(obj.wrap).each(function(){
        var hold = $(this);
        var link = hold.find(obj.link);
        var box = hold.find(obj.box);
        var w = obj.w;
        var close = hold.find(obj.close);

        link.click(function(){
            $(obj.wrap).not(hold).removeClass(obj.openClass);
            if (!hold.hasClass(obj.openClass)) {
                hold.addClass(obj.openClass);
            }
            else {
                hold.removeClass(obj.openClass);
            }
            return false;
        });

        hold.hover(function(){
            $(this).addClass('hovering');
        }, function(){
            $(this).removeClass('hovering');
        });

        $("body").click(function(){
            if (!hold.hasClass('hovering')) {
                hold.removeClass(obj.openClass);
            }
        });
        close.click(function(){
            hold.removeClass(obj.openClass);

            return false;
        });
    });
}
function listCounter() {
    $('.counter-date').each(function () {
        var hold = $(this);
        var dayText, minuteText, hourText;
        var now = new Date();
        hold.countdown(hold.data('finish'), function (event) {
            if(event.strftime('%D')[event.strftime('%D').length-1] >= 2 && event.strftime('%D')[event.strftime('%D').length-1] <= 4) dayText = 'дня';
            if(event.strftime('%D')[event.strftime('%D').length-1] == 1) dayText = 'день';
            if(event.strftime('%D')[event.strftime('%D').length-1] >= 5 && event.strftime('%D')[event.strftime('%D').length-1] <= 9 || event.strftime('%D')[event.strftime('%D').length-2] == 1 || event.strftime('%D')[event.strftime('%D').length-1] == 0) dayText = 'дней';

            if(event.strftime('%H') == 0 || event.strftime('%H') >= 5 && event.strftime('%H') <= 20) hourText = 'часов'
            else if(event.strftime('%H') == 0) hourText = 'час'
            else if(event.strftime('%H') >= 2 && event.strftime('%H') <= 4 || event.strftime('%H') >= 22 && event.strftime('%H') <= 24) hourText = 'часа';


            if(event.strftime('%M').length > 1){
                if(event.strftime('%M') < 21 || event.strftime('%M')[1] >= 5 && event.strftime('%M')[1] <= 9 || event.strftime('%M')[1] == 0) minuteText = 'минут'
                else if (event.strftime('%M')[1] <2) minuteText = 'минутa'
                else if(event.strftime('%M')[1] >= 2 && event.strftime('%M')[1] <= 4)minuteText = 'минуты';
            }else{
                if(event.strftime('%M') == 0 || event.strftime('%M') >= 5 && event.strftime('%M') <= 9) minuteText = 'минут'
                else if(event.strftime('%M') == 1) minuteText = 'минута'
                else if(event.strftime('%M') >= 2 && event.strftime('%M') <= 4)  minuteText = 'минуты'
            }
            hold.find('.day').html('<span class="counter-num">'+event.strftime('%D')+'</span> ' + dayText);
            hold.find('.hour').html('<span class="counter-num">'+event.strftime('%H')+'</span> ' +  hourText );
            hold.find('.minute ').html('<span class="counter-num">'+event.strftime('%M')+ '</span> ' + minuteText);
            hold.find('.second').html(event.strftime('%S')).parent();
        });
    });
}
function resizeMap(){
    $('body').each(function(){
        var hold = $(this);
        var _time;

        function init() {
            if (document.getElementById('map')) {
                var wW = $(window).width();
                var sizeImgW, sizeImgH, imgurl;

                sizeImgW = 37;
                sizeImgH = 56;
                imgurl = 'images/geo.svg';
                var mapOptions = {
                    tileSize: new google.maps.Size(256, 256),
                    zoom: 16,
                    maxZoom: 18,
                    zoomControl: false,
                    streetViewControl: false,
                    scaleControl : false,
                    fullscreenControl : false,
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                        position: google.maps.ControlPosition.TOP_RIGHT,
                        mapTypeIds: ['styled_map', 'satellite', 'hybrid']
                        //mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain','styled_map']
                    },
                    center: new google.maps.LatLng(60.515, 30.211209),
                };
                var styledMapType = new google.maps.StyledMapType(
                    [
                        {"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},
                        {"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#1a1a1a"}]},
                        {"featureType":"landscape","elementType":"all","stylers":[{"color":"#1a1a1a"}]},
                        {"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#2b2b2b"},{"visibility":"on"}]},
                        {"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#1a1a1a"},{"visibility":"on"}]},
                        {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                        {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":0}]},
                        {"featureType":"road","elementType":"geometry","stylers":[{"color":"#000000"},{"saturation":-100},{"lightness":0}]},
                        {"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},
                        {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"on"}]},
                        {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"on"}]},
                        {"featureType":"transit","elementType":"all","stylers":[{"visibility":"on"}]},
                        {"featureType":"water","elementType":"all","stylers":[{"color":"#262626"},{"visibility":"on"}]}
                    ],
                    {name: 'Карта'}
                );
                var mapElement = document.getElementById('map');

                var map = new google.maps.Map(mapElement, mapOptions);
                //Associate the styled map with the MapTypeId and set it to display.
                map.mapTypes.set('styled_map', styledMapType);
                map.setMapTypeId('styled_map');

                var markerImage = new google.maps.MarkerImage(imgurl, new google.maps.Size(sizeImgW, sizeImgH),
                    new google.maps.Point(0, 0));

                if($('#map').length > 0){
                    for (var i = 0; i < locations.length; i++) {
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(locations[i].latlng[0], locations[i].latlng[1]),
                            map: map,
                            icon: markerImage,
                            title: locations[i].title
                        });
                        map.setCenter(new google.maps.LatLng(locations[i].latlng[0], locations[i].latlng[1]));
                    }
                }
            }
        }

        $(window).resize(function(){
            if(_time) clearTimeout(_time);
            _time = setTimeout(function(){
                init();
            }, 500);
        });

        init();
    });
}
function gallerySelect(){
$('.hedges-box').each(function(){
    var hold = $(this);
    var selectors = hold.find(".drop-list li");
    var checkBox = $('#open-dropbox');
    var label = hold.find('.label-dropbox');
    var gallery = hold.find('.slide-hedges');
    var galSlides = gallery.find('li');
    var preloader = hold.find('.preloader');
    var lis;
	var except = 'except';

    selectors.on('click', function (e) {
        e.preventDefault();
        selectors.removeClass('active');
        $(this).addClass('active');
        label.text($(this).find('a').text());
        gallery.gallery('destroy');
        galSlides.removeClass('hidden');
        if($(this).data('visible')!='all' ){
			hideSlides($(this).data('visible'))
        }else if($(this).data('visible')=='all'){
			hideSlides(except, true)
		}
        gallery.gallery({
            oneSlide: true
        });
        preloader.css({
            width: "50%"
        });
        setTimeout(function(){
            checkBox.prop('checked', false);
            preloader.css({
                width: "0"
            });
        },1000)

    });

    selectors.eq(0).trigger('click');

    function hideSlides(selector, isAll){
		var text = isAll ? '.'+selector : ':not(.'+selector+')';
		lis = galSlides.filter(text);
            lis.addClass('hidden');
	}
})
}